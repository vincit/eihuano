'use strict';

const gulp = require('gulp');
const nodemon = require('gulp-nodemon');
const watchify = require('gulp-watchify');

gulp.task('watchify', watchify(watcher => {
    return gulp.src('frontend/js/app.js')
        .pipe(watcher({
            setup: bundle => bundle.transform('babelify', {presets: ['es2015', 'react']})
        }))
        .pipe(gulp.dest('build/'));
}));

gulp.task('start-server', ['watchify'], () => {
    nodemon({
        script: 'backend/app.js',
        ext: 'js'
    });
});
